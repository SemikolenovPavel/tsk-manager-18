package ru.t1.semikolenov.tm.api.repository;

import ru.t1.semikolenov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User removeUser(User user);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeById(String id);

    User removeByLogin(String login);

}
