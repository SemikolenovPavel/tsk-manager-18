package ru.t1.semikolenov.tm.api.service;

public interface IServiceLocator {

    ICommandService getCommandService();

    ITaskService getTaskService();

    IProjectService getProjectService();

    IUserService getUserService();

    IProjectTaskService getProjectTaskService();

    ILoggerService getLoggerService();

    IAuthService getAuthService();

}
