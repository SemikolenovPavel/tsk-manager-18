package ru.t1.semikolenov.tm.exception.entity;

public final class AccessDeniedException extends AbstractEntityNotFoundException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
