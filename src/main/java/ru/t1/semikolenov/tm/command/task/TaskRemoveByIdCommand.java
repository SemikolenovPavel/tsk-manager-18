package ru.t1.semikolenov.tm.command.task;

import ru.t1.semikolenov.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-remove-by-id";

    public static final String DESCRIPTION = "Remove task by id.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().removeById(id);
    }

}
