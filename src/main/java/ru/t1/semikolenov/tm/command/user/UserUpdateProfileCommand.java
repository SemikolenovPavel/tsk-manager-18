package ru.t1.semikolenov.tm.command.user;

import ru.t1.semikolenov.tm.exception.entity.UserNotFoundException;
import ru.t1.semikolenov.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    public static final String NAME = "update-user-profile";

    public static final String DESCRIPTION = "Update profile of current user.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROFILE]");
        String userId = getAuthService().getUserId();
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        System.out.println("ENTER FIRST NAME:");
        String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

}
