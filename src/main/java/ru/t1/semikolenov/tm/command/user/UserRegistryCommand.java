package ru.t1.semikolenov.tm.command.user;

import ru.t1.semikolenov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    public static final String NAME = "user-registry";

    public static final String DESCRIPTION = "Registry new user.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY NEW USER]");
        System.out.println("ENTER LOGIN:");
        String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        String email = TerminalUtil.nextLine();
        getAuthService().registry(login, password, email);
    }

}
