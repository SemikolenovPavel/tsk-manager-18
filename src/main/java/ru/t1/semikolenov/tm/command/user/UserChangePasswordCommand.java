package ru.t1.semikolenov.tm.command.user;

import ru.t1.semikolenov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String NAME = "change-user-password";

    public static final String DESCRIPTION = "Change password of current user.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        final String userId = getAuthService().getUserId();
        System.out.println("ENTER NEW PASSWORD:");
        String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

}
